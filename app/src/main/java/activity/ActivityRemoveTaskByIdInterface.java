package activity;

/**
 * Created by Thul on 30/12/2015.
 */
public interface ActivityRemoveTaskByIdInterface {
    public void onSuccesRemoveTaskById();
    public void onFailRemoveTaskById(String res);
    public int getId();
}
