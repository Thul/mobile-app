package activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.example.simon.mobileproject.R;

import domain.Task;
import domain.TaskList;
import tasks.TaskListTasks.AddTlTask;
import tasks.TaskTasks.AddTtask;
import tasks.TaskTasks.GetAllTByListIdTask;
import tasks.TaskTasks.GetTbyId;
import tasks.TaskTasks.RemoveTbyIdTask;

import java.util.List;

public class TaskDetailActivity extends AppCompatActivity implements ActivityRemoveTaskByIdInterface, AcitivityFindTbyIdInterface, ActivityFindTByListIdInterface {

    private int position;
    private TextView name;
    private TextView description;
    private TextView done;
    private Button doneButton;
    private Button editButton;
    private Button deleteButton;
    private Task task;
    private List<Task> list;
    private Context context;
    private ActivityRemoveTaskByIdInterface activity;
    private AcitivityFindTbyIdInterface activity2;
    private ActivityFindTByListIdInterface activity3;
    private TextView date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_task_detail);
        context = this;
        activity = this;
        activity2 = this;
        activity3 = this;

        readData();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);



    }

    private String dateToUiDate(){
        String date[] = task.getCreationDate().split(" ");
        String res = "Created "+ date[0] + " at " + date[1];
        return res;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                Intent intent = new Intent(this, TaskOverviewActivity.class);
                intent.putExtra("taskListId", task.getTaskList().getId().intValue());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setStatus(){
        if(task.getisChecked()){
            doneButton.setText("In progress");
            done.setText("Done");
            done.setTextColor(Color.GREEN);
        }else{
            doneButton.setText("Done");
            done.setText("In progress");
            done.setTextColor(Color.RED);
        }
    }

    private void readData(){

            Bundle extras = getIntent().getExtras();
            position = extras.getInt("task");
            new GetTbyId(activity2).execute();
    }


    protected void OnResume(){
        super.onResume();
        readData();
    }


    @Override
    public void onSuccesRemoveTaskById() {


        Intent intent = new Intent(context, TaskOverviewActivity.class);
        intent.putExtra("taskListId", task.getTaskList().getId().intValue());
        startActivity(intent);
    }

    @Override
    public void onFailRemoveTaskById(String res) {
        System.out.println(res);
    }

    @Override
    public int getId() {
        return position;
    }

    @Override
    public int getTaskId() {
        return position;
    }

    @Override
    public void onSuccesFindTbyId(final Task t) {
        task = t;
        name = (TextView)findViewById(R.id.detailName);
        description = (TextView)findViewById(R.id.detailDescription);
        done = (TextView)findViewById(R.id.detailStatus);


        name.setText(task.getSubject());
        description.setText(task.getDescription());

        date = (TextView)findViewById(R.id.detailTaskDate);
        date.setText(dateToUiDate());

        doneButton = (Button)findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Task t = Task.load(Task.class, task.getId());
                task.setChecked((task.getisChecked()) ? false : true);
                new AddTtask().execute(task);
                //task.save();

                new GetAllTByListIdTask(activity3).execute(task.getTaskList().getId().intValue());


            }
        });
        setStatus();

        editButton = (Button)findViewById(R.id.editTask);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context ,EditTaskDetailActivity.class);
                intent.putExtra("task", task.getId().intValue());
                startActivity(intent);
            }
        });

        deleteButton = (Button) findViewById(R.id.deleteTaskDetail);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RemoveTbyIdTask(activity).execute();
            }
        });
    }

    @Override
    public void onFailFindTbyId(String res) {
        System.out.println(res);
    }

    @Override
    public void onSuccesFindTbyListId(List<Task> res) {
        list = res;
        boolean result = true;
        for(Task t: list){
            if(!task.getisChecked()){
                result = false;
                break;
            }
        }


        task.getTaskList().setIsDone(result);
        new AddTlTask().execute(task.getTaskList());
        //task.getTaskList().save();


        Intent intent = new Intent(context,TaskOverviewActivity.class);
        intent.putExtra("taskListId", task.getTaskList().getId().intValue());
        startActivity(intent);
    }

    @Override
    public void onFailFindTByListId(String res) {

    }
}
