package activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.example.simon.mobileproject.R;

import java.util.List;

import domain.Task;
import domain.TaskList;
import tasks.TaskListTasks.AddTlTask;
import tasks.TaskListTasks.GetTlByIdTask;
import tasks.TaskListTasks.RemoveTlByIdTask;
import tasks.TaskTasks.GetAllTByListIdTask;

public class TaskListDetailActivity extends AppCompatActivity implements ActivityFindTLbyIdInterface, ActivityRemoveTaskListIdInterface, ActivityFindTByListIdInterface {

    public int position;
    private TextView name;
    private TextView description;
    private TextView ischecked;
    private TextView date;
    private Button done;
    private Button edit;
    private Button delete;
    private List<Task> tasks;
    private ActivityRemoveTaskListIdInterface activity;
    private ActivityFindTByListIdInterface activity2;
    private TaskList taskList;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_tasklist_detail);
        context = this;
        activity = this;
        activity2 = this;
        readData();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String dateToUiDate(){
        String date[] = taskList.getCreationDate().split(" ");
        String res = "Created "+ date[0] + " at " + date[1];
        return res;
    }

    private void setStatus(){
        boolean res = true;

        if(tasks.size() <= 0){
            done.setText("Done");
            done.setEnabled(false);
            ischecked.setText("In progress");
            ischecked.setTextColor(Color.RED);
        }else {
            for (Task task : tasks) {
                if (!task.getisChecked()) {
                    res = false;
                }
            }
            if (res) {
                taskList.setIsDone(true);
                new AddTlTask().execute(taskList);
                done.setEnabled(false);
                ischecked.setText("Done");
                ischecked.setTextColor(Color.GREEN);
            } else {
                taskList.setIsDone(false);
                new AddTlTask().execute(taskList);
                done.setText("Done");
                done.setEnabled(true);
                ischecked.setText("In progress");
                ischecked.setTextColor(Color.RED);
            }
        }
    }

    private void readData(){

        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
       // taskList = TaskList.load(TaskList.class, position);
        new GetTlByIdTask(this).execute();
    }

    @Override
    public void onSucces(TaskList res) {
        taskList = res;
        name = (TextView)findViewById(R.id.detailTaskListName);
        name.setText(taskList.getName().toString());
        description = (TextView) findViewById(R.id.detailTaskListDescription);
        description.setText(taskList.getDescription().toString());
        date = (TextView)findViewById(R.id.detailTaskListDate);
        date.setText(dateToUiDate());
        ischecked = (TextView)findViewById(R.id.detailTaskListProgress);
        new GetAllTByListIdTask(activity2).execute(taskList.getId().intValue());

    }

    @Override
    public void onFail(String res) {
        System.out.println(res);
    }

    @Override
    public int getId() {
        return position;
    }

    @Override
    public void onSuccesRemoveTaskListId() {
        Intent intent = new Intent(context, MainActivity.class);
        //intent.putExtra("taskListId", taskList.getId().intValue());
        startActivity(intent);
    }

    @Override
    public void onFailRemoveTaskListId(String res) {
        System.out.println(res);
    }

    @Override
    public void onSuccesFindTbyListId(List<Task> res) {
        tasks = res;
        done = (Button)findViewById(R.id.detailTaskListDone);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete task")
                        .setMessage("Are you sure u want to check of this task list?" + "\n" +
                                "If so please note that all the tasks of this list will be marked as done.")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (ischecked.getText().toString().equals("In progress")) {
                                    for (Task task : tasks) {
                                        if (!task.getisChecked()) {
                                            task.setChecked(true);
                                            task.save();
                                        }
                                    }
                                    setStatus();
                                }
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        edit = (Button)findViewById(R.id.detailTaskListEdit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditTaskListDetailActivity.class);
                intent.putExtra("taskListId", taskList.getId().intValue());
                startActivity(intent);
            }
        });


        delete = (Button) findViewById(R.id.detailTaskListDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RemoveTlByIdTask(activity).execute();
            }
        });

        setStatus();
    }

    @Override
    public void onFailFindTByListId(String res) {

    }
}
