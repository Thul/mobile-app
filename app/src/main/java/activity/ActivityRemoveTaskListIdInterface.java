package activity;

import domain.TaskList;

/**
 * Created by Thul on 30/12/2015.
 */
public interface ActivityRemoveTaskListIdInterface {
    public void onSuccesRemoveTaskListId();
    public void onFailRemoveTaskListId(String res);
    public int getId();
}
