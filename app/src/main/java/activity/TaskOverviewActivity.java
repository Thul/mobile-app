package activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import adapter.TaskAdapter;
import domain.Task;
import domain.TaskList;
import domain.TaskListManager;
import tasks.TaskListTasks.GetTlByIdTask;
import tasks.TaskTasks.GetAllTByListIdTask;

import com.example.simon.mobileproject.R;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.List;

public class TaskOverviewActivity extends AppCompatActivity implements ActivityFindTLbyIdInterface, ActivityFindTByListIdInterface {

    private TaskListManager tlm;
    private TaskList list;
    private List<Task> tasks;
    ListView lv;
    Context context;
    ActivityFindTByListIdInterface activity;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_task_overview);
        context = this;
        activity = this;
        readData();



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onItemClick(int position){
        Intent intent = new Intent(this, TaskDetailActivity.class);
        intent.putExtra("task", position);
        startActivity(intent);
    }

    private void readData(){
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
        new GetTlByIdTask(this).execute();
    }

    @Override
    protected void onResume(){
        super.onResume();
        readData();
    }

    @Override
    public void onSucces(TaskList res) {
        list = res;
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_task);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddTaskActivity.class);
                intent.putExtra("taskListId", position);
                startActivity(intent);
            }
        });
        new GetAllTByListIdTask(activity).execute(list.getId().intValue());

    }

    @Override
    public void onFail(String res) {
        System.out.println("niet Goed");
    }

    @Override
    public int getId() {
        return position;
    }

    @Override
    public void onSuccesFindTbyListId(List<Task> res) {
        tasks = res;
        AlphaInAnimationAdapter alphaInAnimationAdapter = new AlphaInAnimationAdapter(new TaskAdapter(this, tasks, position));
        lv = (ListView)findViewById(R.id.taskListview);
        alphaInAnimationAdapter.setAbsListView(lv);
        lv.setAdapter(alphaInAnimationAdapter);
    }

    @Override
    public void onFailFindTByListId(String res) {
        System.out.println(res);
    }
}
