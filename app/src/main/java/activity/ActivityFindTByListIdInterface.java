package activity;

import java.util.List;

import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 30/12/2015.
 */
public interface ActivityFindTByListIdInterface {
    public void onSuccesFindTbyListId(List<Task> res);
    public void onFailFindTByListId(String res);

}
