package activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.simon.mobileproject.R;

import domain.TaskList;
import tasks.TaskListTasks.AddTlTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddTaskListActivity extends AppCompatActivity {
    private Context context;
    private EditText input;
    private EditText description;
    //private DbService service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
      //  initalizeService();
        setContentView(R.layout.activity_add_task_list);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.save_task_list);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = (EditText) findViewById(R.id.input);
                description = (EditText)findViewById(R.id.taskListDescription);

                TaskList tl = new TaskList();
                tl.setName(input.getText().toString());
                tl.setDescription(input.getText().toString());
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                tl.setCreationDate(df.format(c.getTime()));
                //int seconds = c.get(Calendar.SECOND);



                new AddTlTask().execute(tl);
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
            }
        });

    }

/*    private void initalizeService() {
        service = new DbService();
    } */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
