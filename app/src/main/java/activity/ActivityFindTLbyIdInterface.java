package activity;

import domain.TaskList;

/**
 * Created by Thul on 30/12/2015.
 */
public interface ActivityFindTLbyIdInterface {
    public void onSucces(TaskList res);
    public void onFail(String res);
    public int getId();
}
