package activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.simon.mobileproject.R;

import domain.TaskList;

import tasks.TaskListTasks.AddTlTask;
import tasks.TaskListTasks.GetTlByIdTask;

public class EditTaskListDetailActivity extends AppCompatActivity implements ActivityFindTLbyIdInterface{

    private EditText name;
    private EditText description;
    private Context context;
    private TaskList taskList;
    private Button save;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_task_list_detail);

        context = this;
        readData();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public int getPostition() {
        return position;
    }


    private void readData() {
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
        GetTlByIdTask task = new GetTlByIdTask(this);
        task.execute();
    }

    @Override
    public void onSucces(TaskList res) {
        taskList = res;
        name = (EditText)findViewById(R.id.editTaskListDetailName);
        description = (EditText)findViewById(R.id.editTaskListDetailDescription);
        name.setHint(taskList.getName());
        description.setHint(taskList.getDescription());
        save = (Button)findViewById(R.id.editTaskListSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ik geraak in de onclick en list:" + taskList.getName());
                taskList.setName((name.getText().toString().isEmpty()) ? taskList.getName() : name.getText().toString());
                taskList.setDescription((description.getText().toString().isEmpty()) ? taskList.getDescription() : description.getText().toString());
                new AddTlTask().execute(taskList);
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onFail(String res) {
        for(int i = 0 ; i < 50; i++) {
            System.out.println(res);
        }
    }

    @Override
    public int getId() {
        return position;
    }

}
