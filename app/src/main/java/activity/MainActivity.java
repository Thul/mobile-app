package activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.activeandroid.ActiveAndroid;

import adapter.TaskListAdapter;
import domain.TaskList;
import domain.TaskListManager;
import tasks.TaskListTasks.GetAllTlTask;

import com.example.simon.mobileproject.R;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;


import java.util.List;

public class MainActivity extends AppCompatActivity  {


    public TaskListManager tlm;
    ListView lv;
    Context context;
    List<TaskList> lists;
    TaskListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_main);
        context = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddList);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, AddTaskListActivity.class);
                startActivity(i);
            }
        });

        new GetAllTlTask(this).execute();
    }

    public void taskSucces(List<TaskList> res) {
        lists = (List<TaskList>)res;
        tlm = new TaskListManager(lists);

        lv = (ListView) findViewById(R.id.listview);

        adapter = new TaskListAdapter(this, tlm.gettTaskLists());

        AlphaInAnimationAdapter animAdapter = new AlphaInAnimationAdapter(adapter);
        animAdapter.setAbsListView(lv);
        lv.setAdapter(animAdapter);
    }

    public void taskFail(String res) {
        for(int i = 0 ; i < 50; i++) {
            System.out.println(res);
        }
    }

    public void onItemClick(int position){
        Intent intent = new Intent(this, TaskOverviewActivity.class);
        intent.putExtra("taskListId", position);
        startActivity(intent);
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    protected void onPause(){
        super.onPause();

    }


}
