package database;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import java.util.List;

import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public class DbTaskListSQL implements DbTaskListInterface {

    public DbTaskListSQL() {

    }

    @Override
    public List<TaskList> getTaskLists() {
        return new Select()
                .from(TaskList.class)
                .orderBy("is_done")
                .execute();
    }

    @Override
    public TaskList getTaskList(int id) {
        return new Select()
                .from(TaskList.class)
                .where("id=="+id)
                .executeSingle();
    }


    @Override
    public void addTaskList(TaskList list) {
        if(list != null) list.save();
    }

    @Override
    public void removeTaskList(int id) {
        TaskList.delete(TaskList.class, id);
    }

    @Override
    public void updateTaskList(TaskList list) {
        if(list != null) list.save();
    }

    @Override
    public List<Task> getTasks(int id) {
        return new Select().from(Task.class)
                .where("TaskList ==" + id)
                .orderBy("IsChecked").execute();
    }
}
