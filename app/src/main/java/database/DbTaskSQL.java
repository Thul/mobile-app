package database;

import com.activeandroid.query.Select;

import java.util.List;

import domain.Task;

/**
 * Created by Thul on 29/12/2015.
 */
public class DbTaskSQL implements DbTaskInterface {
    @Override
    public List<Task> getTaskList(int id) {
        return new Select().from(Task.class)
                .where("TaskList ==" + id)
                .orderBy("IsChecked").execute();
    }

    @Override
    public void addTask(Task task) {
        task.save();
    }

    @Override
    public void removeTask(int id) {
        Task.delete(Task.class, id);
    }

    @Override
    public void updateTask(Task task) {
        task.save();
    }

    @Override
    public Task getTaskById(int id) {
        return Task.load(Task.class, id);
    }

}
