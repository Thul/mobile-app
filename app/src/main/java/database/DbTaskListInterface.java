package database;

import java.util.List;

import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public interface DbTaskListInterface {

    public TaskList getTaskList(int id);
    public List<TaskList> getTaskLists();
    public void addTaskList (TaskList list);
    public void removeTaskList(int id);
    public void updateTaskList(TaskList list);
    public List<Task>getTasks(int id);


}
