package tasks.TaskTasks;

import android.os.AsyncTask;

import com.activeandroid.query.Select;

import java.util.List;

import activity.ActivityFindTByListIdInterface;
import activity.ActivityFindTLbyIdInterface;
import database.DbTaskInterface;
import database.DbTaskSQL;
import domain.Task;

/**
 * Created by Thul on 30/12/2015.
 */
public class GetAllTByListIdTask extends AsyncTask<Integer, Void, Boolean> {

    ActivityFindTByListIdInterface context;
    List<Task> tasks;

    public GetAllTByListIdTask(ActivityFindTByListIdInterface context) {
        this.context =  context;
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        try {
            DbTaskInterface db = new DbTaskSQL();
            tasks = db.getTaskList(params[0]);
            return  true;
        } catch (Exception e) {
            return false;
        }
     }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            context.onSuccesFindTbyListId(tasks);
        } else {
            context.onFailFindTByListId("faal");
        }
    }
}
