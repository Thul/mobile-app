package tasks.TaskTasks;

import android.os.AsyncTask;

import activity.AcitivityFindTbyIdInterface;
import activity.ActivityFindTLbyIdInterface;
import database.DbTaskInterface;
import database.DbTaskSQL;
import domain.Task;

/**
 * Created by Thul on 30/12/2015.
 */
public class GetTbyId extends AsyncTask<Void, Void, Boolean> {

    private AcitivityFindTbyIdInterface context;
    private DbTaskInterface db;
    Task res;

    public GetTbyId(AcitivityFindTbyIdInterface context) {
        this.context = context;
        db = new DbTaskSQL();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            res = db.getTaskById(context.getTaskId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(aBoolean) {
            context.onSuccesFindTbyId(res);
        } else {
            context.onFailFindTbyId("Uw ma");
        }
    }
}
