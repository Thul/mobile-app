package adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


import activity.MainActivity;
import com.example.simon.mobileproject.R;
import activity.TaskListDetailActivity;

import domain.TaskList;
import tasks.TaskListTasks.RemoveTlByIdTask;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */
public class TaskListAdapter extends BaseAdapter implements View.OnClickListener  {

    private Activity activity;
    private List<TaskList> data;
    private static LayoutInflater inflater = null;
    public Resources res;
    TaskList tempValues = null;
    int i = 0;



    public TaskListAdapter(Activity activity, List<TaskList> data){
        this.activity = activity;
        this.data = data;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data.size() <= 0)
            return 0;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class Holder{
        TextView taskListSubject;
        Button detailButton;
        CheckBox done;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        Holder holder;

        if(convertView == null){
            vi = inflater.inflate(R.layout.manager_list_item, null);

            holder = new Holder();
            holder.taskListSubject = (TextView)vi.findViewById(R.id.managerListSubject);
            holder.detailButton = (Button)vi.findViewById(R.id.taskListDetail);
            holder.done = (CheckBox)vi.findViewById(R.id.taskListCheck);

            vi.setTag(holder);
        }

        else
            holder = (Holder)vi.getTag();

        if(data.size() <= 0){

        }else{
            tempValues = null;
            tempValues = (TaskList)data.get(position);
            holder.taskListSubject.setText(tempValues.getName());
            holder.detailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tempValues = (TaskList) data.get(position);
                    Intent intent = new Intent(activity, TaskListDetailActivity.class);
                    intent.putExtra("taskListId", tempValues.getId().intValue());
                    activity.startActivity(intent);

                }
            });

            holder.done.setChecked(tempValues.isDone());
            vi.setOnClickListener(new OnItemClickListener(new BigDecimal(tempValues.getId()).intValue()));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("TaskListAdapter", "----------------row button clicked -------------");
    }


    private class OnItemClickListener implements View.OnClickListener {
        private int pos;

        OnItemClickListener(int position){
            pos = position;
        }

        @Override
        public void onClick(View arg0){
           MainActivity mainActivity = (MainActivity)activity;
            mainActivity.onItemClick(pos);

        }
    }

}
