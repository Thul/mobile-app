package adapter;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.simon.mobileproject.R;
import activity.TaskOverviewActivity;

import domain.Task;
import domain.TaskList;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */
public class TaskAdapter extends BaseAdapter implements View.OnClickListener  {


    private Activity activity;
    private List<Task> data;
    private static LayoutInflater inflater = null;
    public Resources res;
    Task tempValues = null;
    int i = 0;
    int id;

    public TaskAdapter(Activity a, List<Task> d, int id){
        activity = a;
        data = d;
        this.id = id;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data != null){
            if (data.size() <= 0) {
                return 0;
            }
            return data.size();

        }else{
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    /************ Holder class die de inflated xml file elements bijhoud *********/
    public class Holder{
        TextView taskSubject;
        TextView taskDescription;
        CheckBox taskChecked;
    }

    public View getView(final int position, final View convertView, ViewGroup parent){
        View vi = convertView;

        Holder holder;

        if(convertView == null){
            /***** inflate tasklistitem.xml for each row ******/
            vi = inflater.inflate(R.layout.task_list_item, null);

            holder = new Holder();
            holder.taskSubject = (TextView) vi.findViewById(R.id.taskSubject);
            holder.taskDescription = (TextView) vi.findViewById(R.id.taskDescription);

            holder.taskChecked = (CheckBox) vi.findViewById(R.id.taskListItemCheck);

            vi.setTag(holder);
        }else
            holder =(Holder)vi.getTag();

            if(data.size() <= 0){

            }
        else{
                tempValues = null;
                tempValues = (Task) data.get(position);

                holder.taskSubject.setText(tempValues.getSubject());
                holder.taskDescription.setText(tempValues.getDescription());
                holder.taskChecked.setChecked(tempValues.getisChecked());
                System.out.println(tempValues.getId());
                vi.setOnClickListener(new OnItemClickListener(new BigDecimal(tempValues.getId()).intValue()));
            }
        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("TaskListAdapter", "----------------row button clicked -------------");
    }


    private class OnItemClickListener implements View.OnClickListener {
        private int pos;

        OnItemClickListener(int position){
            pos = position;
        }

        @Override
        public void onClick(View arg0){
            TaskOverviewActivity taskOverviewActivity = (TaskOverviewActivity)activity;
            taskOverviewActivity.onItemClick(pos);
        }
    }



}
