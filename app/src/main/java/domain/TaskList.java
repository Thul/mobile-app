package domain;



import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import domain.Task;

import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */

@Table(name = "task_list")
public class TaskList extends Model {

    @Column(name="Name")
    private String name;
    @Column(name="Description")
    private String description;
    @Column(name="Creation_date")
    private String creationDate;
    @Column(name="is_done")
    private boolean isDone;

    public List<Task> tasks;




    public TaskList(){
        super();
    }

    public TaskList(String name, String description){
        super();
        this.name = name;
        this.description = description;
        this.isDone = false;


    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }







}
