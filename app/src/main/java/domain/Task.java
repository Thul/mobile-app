package domain;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by simon on 11/25/2015.
 */
@Table(name="Tasks")
public class Task extends Model {

    @Column(name="Sub")
    private String subject;
    @Column(name = "Description")
    private String description;

    @Column(name="IsChecked")
    private boolean isChecked;

    @Column(name="Creation_date")
    private String creationDate;

    @Column(name="Location")
    private String location;

    @Column(name="TaskList", onDelete = Column.ForeignKeyAction.CASCADE)
    TaskList list;


    public Task(){
        super();
    }

    public Task(String subject, String description){
        this.subject = subject;
        this.description = description;
        this.isChecked = false;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public void setChecked(boolean checked){
        this.isChecked = checked;
    }

    public boolean getisChecked(){
        return isChecked;
    }

    public TaskList getTaskList(){
        return this.list;
    }

    public void setTaskList(TaskList taskList){
        this.list = taskList;
    }


}
